var express = require('express');
var router = express.Router();

router.get('/v1', function(req, res, next) {
   res.send('Redirecting to home page');
});

    
    

router.get('/', function(req, res, next) {
    return res.redirect('/v1')});

module.exports = router;
